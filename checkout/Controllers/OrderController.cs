﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using checkout.Models;

namespace checkout.Controllers
{
    public class OrderController : Controller
    {
        public IActionResult Index()
        {
            var order = new Orders()
            {
                Product1 = "Coffee",
                Product2 = "Biscuit",
                Product3 = "Milk",
                Product4 = "Chocolates"
            };
            return View(order);
        }
        public IActionResult LastPage()
        {
            return View();
        }
    }
}